% Kratos (Κράτος), spirit of strength, might, power, and sovereign rule
function kratos()

    names = getNames();

    while true
        str = input('>','s');

        cmd = strsplit(str);

        switch cmd{1}
        case {'list','ls'} % List all students
            list(names);
        case 'print' % Print all comments
            printComments(names);
        case {'comment', 'c'} % View and comment on a student
            if length(cmd) ~= 2
                fprintf('Provide an id to view and comment!\n')
                continue;
            end

            I = sscanf(cmd{2},'%d');

            examineFile(names{I})
        case 'all' % Go through all students
            for i = 1:length(names)
                examineFile(names{i})
            end
        case 'from'
            if length(cmd) ~= 2
                fprintf('Provide an id to start from!\n')
                continue;
            end

            I = sscanf(cmd{2},'%d');
            for i = I:length(names)
                examineFile(names{i})
            end

        case {'allmissing', 'miss', 'missing', 'am'} % Go through all students without comments
            hc = hasComments(names);
            toRun = names(~hc);
            toRun
            for i = 1:length(toRun)
                examineFile(toRun{i})
            end

        case 'stop' % Stop the tool and return to matlab prompt
            break;

        case 'help' % Print a not so helpful help message
            fprintf('Commands: list print comment all allmissing stop help\nRead the source for more info! :D\n')
        otherwise
            fprintf('Unknown command\n');
        end
    end
end

function list(names)
    for i = 1:length(names)
        if exist(['./' names{i} '/comment.txt'], 'file')
            c = 'x';
        else
            c = ' ';
        end

        fprintf('%2d[%s]: %s\n', i, c, names{i});
    end
end

function o = hasComments(names)
    n = length(names);
    hc = zeros(1,n);

    for i = 1:n
        hc(i) = exist(['./' names{i} '/comment.txt'], 'file');
    end

    o = logical(hc);
end

function printComments(names)
    n = length(names);

    for i = 1:n
        filename = ['./' names{i} '/comment.txt'];
        if exist(filename, 'file')
            f = fopen(filename, 'r');
            b = fread(f);

            fprintf('%s:\n%s\n\n',names{i},b);
        end

    end
end

function names = getNames()
    contents = dir('.');

    names = {};

    for item = contents'
        name = item.name;

        if strcmp(name, '.') || strcmp(name, '..')
            continue
        end

        if ~item.isdir
            continue
        end

        names{end+1} = name;
    end
end


function examineFile(name)
    isScript=whichAreScripts(name);
    nScripts=sum(isScript);
    mfiles = dir(['./' name '/*.m']);
    fprintf('===================================\n');
    fprintf('Namn: %s\n', name);
    typeAllmFilesInCommandWindow(name,mfiles);
    fprintf('===================================\n\n\n');
    if nScripts==0
        fprintf('No m-file to run for %s.\n', name)
    elseif nScripts>1
        fprintf('More than one script in folder.\n')
    else
        mfileToRun=mfiles(isScript);
        runAndCatch([name '/' mfileToRun.name]);
    end
    fprintf('\n\n===================================\n');

    promptComment(name)
    close all;
end

function[]=typeAllmFilesInCommandWindow(name,mfiles)
for i=1:length(mfiles)
    fprintf('= = = = = = = = = = = = = = = = = =\n');
    type([name '/' mfiles(i).name])
end
end

function[isScript]=whichAreScripts(name)
allmfiles = dir(['./' name '/*.m']);
isFunction=false(length(allmfiles),1);
for i=1:length(allmfiles)
    mfile=allmfiles(i);
    isFunction(i)=mfileIsFunction([name '/' mfile.name]);
end
isScript=~isFunction;
end

function[isFunction]=mfileIsFunction(fullmfilepath)
contents=textread(fullmfilepath,'%s','delimiter','\n');
isFunction=false;
rowindex=1;
expressionToMatch='function.*=.+\(.*\)';
while((~isFunction) && (rowindex<length(contents)))
    row=contents{rowindex};
    isFunction=~isempty(regexp(row,expressionToMatch, 'once'));
    rowindex=rowindex+1;
end
end

function promptComment(name)
    commandwindow;
    comment = input('? ', 's');
    writeComment(name, comment)
end

function writeComment(name, comment)
    if length(comment) > 1
        f = fopen([name '/comment.txt'], 'w');
        fwrite(f, comment);
        fclose(f);
    end
end

function runAndCatch(filnamn)
    try
        run(filnamn)
    catch e
        disp(e.message)
    end
end
